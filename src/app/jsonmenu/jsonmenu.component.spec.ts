import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonmenuComponent } from './jsonmenu.component';

describe('JsonmenuComponent', () => {
  let component: JsonmenuComponent;
  let fixture: ComponentFixture<JsonmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsonmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
