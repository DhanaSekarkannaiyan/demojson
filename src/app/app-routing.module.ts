import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JsonmenuComponent } from './jsonmenu/jsonmenu.component';

const routes: Routes = [
  {path:'Jsonmenu',component:JsonmenuComponent},
  {path:'',redirectTo:'Jsonmenu',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 




  
}
